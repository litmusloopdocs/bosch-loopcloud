# bosch-loopmq

This project is to demonstrate the implementation of MQTT. **Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication and plays and important role in the internet of things. MQTT works on the **TCP/IP connection**.This project is to demonstrate the implementation of MQTT. **Message Queue Telemetry Transport (MQTT)** is an extremely simple and lightweight messaging protocol, designed for constrained devices and low bandwidth, high latency and unreliable networks. The protocol uses **publish/subscribe** communication pattern and is used for machine to machine communication and plays and important role in the internet of things. MQTT works on the **TCP/IP connection**.

The **bosch  XDK** is equippd with 7 physical sensors: Accelerometer, Gyroscope, Magnetometer, Inertial (Accelerometer and Gyroscope),Environmental (Pressure,Humidity, and Temperature),Ambient Noise, and Temperature. It has wireless and bluetooth for communication.


## Overview


The library provides an example of publish and subscribe messaging with a server that supports MQTT using Arduino yun.
 
***Features provided by the client library:***

* Connect the device to any IP network using Ethernet, Wi-Fi, 4G/LTE
* Publish any message to the MQTT server in standard JSON format on a specific topic 
* Subscribe data from the server to the device on a specific topic
* Unsubscribe the topic to no longer communicate to the device
* Disconnect the device from any network connectivity.


***The following Table shows status of the client and the server when the above functions are implemented:***

> |Function |  Server Status   |    Client Status
> ----------------|------------------|---------------
|Looopmq.connect | Connected       | Connected|
|Loopmq.publish |Connected |  Connected|
|Loopmq.subscribe | Connected | Connected|
|Loopmq.unsubscribe | Connected | Disconnected|
|Loopmq.disconnect |  Disconnected |  Disconnected|


## Getting Started With BOSCH XDK


>* Open the *CFG_parser_ih.h* in the *source* folder.
* The parameter to connect the device to the internet will be defined on the top of the file. Enter your parameters for the wireless network and MQTT broker.

```
// Default Network Configuration Settings
#define	CFG_DEFAULT_WLAN_SSID	  		"LitmusAutomation_IoT"      	  /**< WLAN SSID Name by Default*/
#define	CFG_DEFAULT_WLAN_PWD	  		"9974287957"   		  /**< WLAN PWD by Default*/
#define CFG_DEFAULT_MQTT_BROKER_NAME    "loopdocker1.cloudapp.net"          /**< MQTT Broker by Default*/
#define CFG_DEFAULT_MQTT_PORT           1883                      /**< MQTT Port Number by Default*/
```

* The data collection settings is below the default network settings.One can edit the rate of each data stream and which sensors are enabled to be monitored.

```
// Default Data Configuration Settings
#define CFG_DEFAULT_STREAM_RATE         1000                      /**< Stream Data Rate by Default*/
#define CFG_DEFAULT_REPORT_SAMPLES      10                        /**< Data Average Report Samples by Default*/
#define CFG_DEFAULT_CONFIG_RATE         120000                    /**< Coniguration Data Rate by Default*/
#define CFG_DEFAULT_GROUP_ID            "Default"                 /**< Devices Group ID by Default*/
#define CFG_DEFAULT_ACCEL_EN            CFG_STR_TRUE              /**< Accelerometer Data Enable by Default*/
#define CFG_DEFAULT_GYRO_EN             CFG_STR_TRUE              /**< Gyroscope Data Enable by Default*/
#define CFG_DEFAULT_MAG_EN              CFG_STR_TRUE              /**< Magnetometer Data Enable by Default*/
#define CFG_DEFAULT_TEMP_EN             CFG_STR_TRUE              /**< Temperature Data Enable by Default*/
#define CFG_DEFAULT_HUM_EN              CFG_STR_TRUE              /**< Humidity Data Enable by Default*/
#define CFG_DEFAULT_PRES_EN             CFG_STR_TRUE              /**< Pressure Data Enable by Default*/
#define CFG_DEFAULT_LIGHT_EN            CFG_STR_TRUE              /**< Ambient Light Data Enable by Default*/
```

* Connect the XDK to the computer with XDK workbench using USB cable. To ensure the XDK comes up in bootloader mode press and hold the button 1 and then turn on the switch.
* The program will run automatically after flashing the device. The yellow LED will turn on during power up and remain on while the XDK connects to the Broker. The yellow LED will turn off and the orange LED will turn on once the connection is established.

  >**NOTE**
    If the yellow LED remains on or if the RED LED turn on, check your network settings and confirm WLAN and MQTT parameters on the XDK are correct.

* The topics wherre the data will be published and subscribed should be defined in *LOOPMQ_Client_ch.h* under the *source* folder.

```
/* Defining Publish Topics */
#define PUB_TOPIC_1             "loop1"
#define PUB_TOPIC_2             "loop2"
#define PUB_TOPIC_3             "loop3"

/* Defining Subscription Topics */
#define SUB_TOPIC1              "loop4"
#define SUB_TOPIC2              "loop5"

```