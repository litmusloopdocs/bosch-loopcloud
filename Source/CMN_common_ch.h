/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	CMN_common_ch.h
**
**	DESCRIPTION:	Configuration header for the common definitions
**
**	PURPOSE:        Contains the public macro, typedef, variables and function
**	                definitions for the entire project
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2016.Jan         crk        BCDS         Consolidation/Cleanup of Code
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef CMN_COMMON_CH_H_
#define CMN_COMMON_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define CMN_NUMBER_UINT8_ZERO		     UINT8_C(0)     /**< Zero value */
#define CMN_NUMBER_UINT32_ZERO 		     UINT32_C(0)    /**< Zero value */
#define CMN_NUMBER_UINT16_ZERO 		     UINT16_C(0)    /**< Zero value */
#define CMN_NUMBER_INT16_ZERO 		     INT16_C(0)     /**< Zero value */

#define CMN_INCOMMING_MSG_LENGTH	 	 UINT8_C(10)   /**< The maximum input command size*/
#define CMN_POINTER_NULL 			     NULL          /**< ZERO value for pointers */

#define CMN_NUMBER_ONE 			         UINT8_C(1)    /**< One value  */

#define CMN_TRUE         			     UINT8_C(1)    /**< One value  */
#define CMN_FALSE     			         UINT8_C(0)    /**< Zero value */
#define CMN_FLOAT_ZERO 			         ((float)0.0)  /**< Zero value for floating numbers*/
#define CMN_DWORD_PARAM_SIZE             UINT8_C(4)    /**< Param size for example when requestting a set timer */
#define CMN_WORD_PARAM_SIZE              UINT8_C(2)    /**< Param size for example when requestting a set bandwith */
#define CMN_CLIENT_CMD_PARAM_SIZE_MAX    UINT8_C(4)    /**< Max Param size for client command requests */
#define CMN_CLIENT_CMD_SIZE              UINT8_C(2)    /**< Client request commands code size */

#define CMN_OUTGOING_MSG_LENGTH		 	 UINT8_C(20)        /**< The maximum input command size*/
#define CMN_ONESECONDDELAY 				 UINT32_C(1000)	 	/**<  one second is represented by this macro */
#define CMN_TIMERBLOCKTIME  			 UINT32_C(0xffff) 	/**<  Macro used to define blocktime of a timer*/
#define CMN_CALCULATION_WINDOW_PERIOD    20000              /**< Time in milli seconds used to make the calculation */
#define CMN_FASTEST_TRANSMISSION_SPEED	 100                /**< The minimum outgoing transmission speed */

#define CMN_TIMER_AUTO_RELOAD_ON         1                  /**< Turn on Auto Reload of the timers */
#define CMN_TIMER_AUTO_RELOAD_OFF        0                  /**< Turn off Auto Reload of the timers */

#define CMN_DELAY_MOUNT                  0
#define CMN_MOUNT_NOW                    1
#define DEFAULT_LOGICAL_DRIVE            ""

#ifndef UNUSED_PARAMETER
	#define UNUSED_PARAMETER(P)                                     (P = P)
#endif

#if !defined( DBG_ASSERT_FILENAME )
	#define DBG_ASSERT_FILENAME __FILE__
#endif

#define SWAP_16_MACRO(x) \
         ((uint16_t)((((uint16_t)(x) & 0x00ff) << 8) | \
        (((uint16_t)(x) & 0xff00) >> 8)))

#define SWAP_32_MACRO(x) \
	    (uint32_t)((x & 0xff) << 24 |	\
	    (x & 0xff00) << 8 |				\
	    (x & 0xff0000) >> 8 |			\
	    (x & 0xff000000) >> 24);

#define SWAP_16_MEMCPY_MACRO(bufAddress,data) \
		{int16_t _tmp = SWAP_16_MACRO(data);\
		memcpy(bufAddress,&_tmp,sizeof(_tmp));}

#define SWAP_32_MEMCPY_MACRO(bufAddress,data) \
		{uint32_t _tmp = SWAP_32_MACRO(data);\
		memcpy(bufAddress,&_tmp,sizeof(_tmp));}

#ifdef XDK_SERIAL_TRACE

	#define traceDebugInfo1(fmt, a)  		printf("@@@ INFO:<%s>\r\n@@@ " fmt ,__FUNCTION__, a)
	#define traceDebugInfo2(fmt, a,b)  		printf("@@@ INFO:<%s>\r\n@@@ " fmt ,__FUNCTION__, a, b)
	#define traceDebugInfo3(fmt, a,b,c) 	printf("@@@ INFO:<%s>\r\n@@@ " fmt ,__FUNCTION__, a, b, c)
	#define traceDebugInfoNoArg(fmt)       	printf("@@@ INFO:<%s>\r\n@@@ " fmt ,__FUNCTION__)

	#define traceDebugError1(fmt,a) 		printf("@@@ ERROR:<%s>\r\n@@@ " fmt ,__FUNCTION__, a)
	#define traceDebugError2(fmt,a,b) 		printf("@@@ ERROR:<%s>\r\n@@@ " fmt ,__FUNCTION__, a, b)
	#define traceDebugError3(fmt, a,b,c)	printf("@@@ ERROR:<%s>\r\n@@@ " fmt ,__FUNCTION__, a, b, c)
	#define traceDebugErrorNoArg(fmt)       printf("@@@ ERROR:<%s>\r\n@@@ " fmt ,__FUNCTION__)
#else

	#define traceDebugInfo1(fmt, a)
	#define traceDebugInfo2(fmt, a,b)
	#define traceDebugInfo3(fmt, a,b,c)
	#define traceDebugInfoNoArg(fmt)

	#define traceDebugError1(fmt,a)
	#define traceDebugError2(fmt,a,b)
	#define traceDebugError3(fmt, a,b,c)
	#define traceDebugErrorNoArg(fmt)

#endif

#define MULTIDATA_FLOAT_XYZ_ZERO    {.f.xyz = {CMN_FLOAT_ZERO,CMN_FLOAT_ZERO,CMN_FLOAT_ZERO}}
#define MULTIDATA_FLOAT_PHT_ZERO    {.f.pht = {CMN_FLOAT_ZERO,CMN_FLOAT_ZERO,CMN_FLOAT_ZERO}}

/* ZERO values for multiData_t */
#define MULTIDATA_INT_XYZ_ZERO        {.i.xyz = {CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO}}
#define MULTIDATA_INT_PHT_ZERO        {.i.pht = {CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO}}

/* Initial values for calculating MIN for multiData_t */
#define MULTIDATA_INT_XYZ_MIN_CALC    {.i.xyz = {INT32_MAX,INT32_MAX,INT32_MAX}}
#define MULTIDATA_INT_PHT_MIN_CALC    {.i.pht = {INT32_MAX,UINT32_MAX,UINT32_MAX}}

/* Initial values for calculating MAX for multiData_t*/
#define MULTIDATA_INT_XYZ_MAX_CALC    {.i.xyz = {INT32_MIN,INT32_MIN,INT32_MIN}}
#define MULTIDATA_INT_PHT_MAX_CALC    {.i.pht = {INT32_MIN,CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO}}

/**
 * brief The transmission method used to perform the communication with the client
 */
typedef enum TransMethod
{
	DTA_SEND_NOTDEFINED      = 0,         /**< No transmission method defined yet */
	DTA_SEND_BLE             = 1,         /**< Transmission method BLE */
	DTA_SEND_USB             = 2,         /**< Transmission method USB */
	DTA_SEND_WIFI            = 3,         /**< Transmission method WIFI */
} transMethod_t;


/* local function prototype declarations */

/* local module global variable declarations */

/* local inline function definitions */

#endif /* CMN_COMMON_CH_H_ */

/** ************************************************************************* */
