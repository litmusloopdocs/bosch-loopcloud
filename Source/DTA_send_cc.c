/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	DTA_send_cc.c
**
**	DESCRIPTION:	Manages the Sensor Data
**
**	PURPOSE:        Initializes the sensors and data buffers,
**					reads the sensor data, caclulates the report data,
**					and writes the data into a JSON formatted buffer
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2015.Oct         crk        BCDS         Edited to work only with wifi, to send
**                                           JSON formated data stream, updated the buffer
**                                           structure and removed unessary functions
**  2016.Jan         crk        BCDS         Added Report (Averaging) stream,
**                                           Configuration stream, and clean up the code
**
*******************************************************************************/

/* module includes ********************************************************** */

/* system header files */
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

/* interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "BCDS_PowerMgt.h"
#include "XdkSensorHandle.h"
#include "BCDS_Retcode.h"

/* other header files */
#include "sl_common.h"

/* own header files */
#include "CMN_common_ch.h"
#include "CFG_parser_ih.h"
#include "DTA_send_ih.h"
#include "DTA_send_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* Data Buffers */
DataBuffer g_dtaStreamBuffer;
DataBuffer g_dtaReportBuffer;
DataBuffer g_dtaConfigBuffer;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaInitializeAccel(void)
{
	Retcode_T _accelReturnValue_BMA280_S = RETCODE_FAILURE;

	_accelReturnValue_BMA280_S = Accelerometer_init(xdkAccelerometers_BMA280_Handle);
	if(RETCODE_SUCCESS != _accelReturnValue_BMA280_S) {
		printf("accelerometerInit BMA280 failed = %ld \r\n",_accelReturnValue_BMA280_S);
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaInitializeGyro(void)
{
	Retcode_T _gyroG160ReturnValue = RETCODE_FAILURE;

	_gyroG160ReturnValue = Gyroscope_init(xdkGyroscope_BMG160_Handle);
	if(RETCODE_SUCCESS != _gyroG160ReturnValue) {
		printf("gyroscopeInit BMG160 failed = %ld \r\n",_gyroG160ReturnValue);
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaInitializeMag(void)
{
	Retcode_T _magnetoReturnValue = RETCODE_FAILURE;

	_magnetoReturnValue = Magnetometer_init(xdkMagnetometer_BMM150_Handle);
	if(RETCODE_SUCCESS != _magnetoReturnValue) {
		printf("magnetometerInit BMM150 failed = %ld \r\n",_magnetoReturnValue);
	}

	if(RETCODE_SUCCESS == _magnetoReturnValue  ) {
		_magnetoReturnValue = dtaPostInitMagSensor();
	}

	if(RETCODE_SUCCESS != _magnetoReturnValue) {
		printf("dtaPostInitMagSensor BMM150 failed = %ld \r\n",_magnetoReturnValue);
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaInitializeLight(void)
{
	Retcode_T _lgtReturnValue = RETCODE_FAILURE;

	_lgtReturnValue = LightSensor_init(xdkLightSensor_MAX44009_Handle);
	if(RETCODE_SUCCESS != _lgtReturnValue) {
		printf("lightsensorInit MAX09 failed = %ld \r\n",_lgtReturnValue);
	}

	if(RETCODE_SUCCESS == _lgtReturnValue) {
		_lgtReturnValue = dtaPostInitLightSensor();
	}

	if(RETCODE_SUCCESS != _lgtReturnValue) {
		printf("dtaPostInitLightSensor MAX09 failed = %ld \r\n",_lgtReturnValue);
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaInitializeEnv(void)
{
	Retcode_T _envReturnValue = RETCODE_FAILURE;

	_envReturnValue = Environmental_init(xdkEnvironmental_BME280_Handle);
	if(RETCODE_SUCCESS != _envReturnValue) {
		printf("environmentalInit BME280 failed = %ld \r\n",_envReturnValue);
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static Retcode_T dtaPostInitLightSensor(void)
{
	Retcode_T _res = RETCODE_FAILURE;

    /** Manual Mode should be enable in order to configure the continuous mode, brightness  and integration time*/
	_res = LightSensor_setManualMode(xdkLightSensor_MAX44009_Handle, LIGHTSENSOR_MAX44009_ENABLE_MODE);

    return _res;
}

/**
 * @brief in DTA_send_ch.h header
 */
static Retcode_T dtaPostInitMagSensor(void)
{
	Retcode_T _res = RETCODE_FAILURE;
	_res = Magnetometer_setPowerMode(xdkMagnetometer_BMM150_Handle,
			MAGNETOMETER_BMM150_POWERMODE_NORMAL);
	return _res;
}

//#ifdef DTA_TEMP_OFFSET
///**
// * @brief in DTA_send_ch.h header
// */
//static int32_t dtaTempOffset(int32_t temperature)
//{
//	/**** Initialize Variables ****/
//	uint32_t _currentTime = OS_timeGetSystemTime();
//	int32_t _tempTemperature = temperature;
//
//	/**** Offset the Read Temerature Value ****/
//	if((_currentTime > DTA_OFFSET_EQ_END_TIME) || s_dtaOffsetTimeFlag) {
//		_tempTemperature -= DTA_OFFSET_POST_EQ;
//		s_dtaOffsetTimeFlag = 1;
//	}
//	else if(_currentTime < DTA_OFFSET_EQ_START_TIME) {
//		_tempTemperature -= DTA_OFFSET_PRE_EQ;
//	}
//	else {
//		_tempTemperature -= DTA_OFFSET_COEFFICIENT * (pow((double)_currentTime, DTA_OFFSET_POWER));
//	}
//
//	if (DTA_POWER_SOURCE == USB_POWER) {
//		_tempTemperature -= DTA_OFFSET_CHARGING;
//	}
//
//	return _tempTemperature;
//}
//#endif

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaAvgAccel(Accelerometer_XyzData_T accelData)
{
	/**** Insert Read Data into Data structure ****/
	s_dtaAccelDataX.CurrentData = accelData.xAxisData;
	s_dtaAccelDataY.CurrentData = accelData.yAxisData;
	s_dtaAccelDataZ.CurrentData = accelData.zAxisData;

	/**** Calculate the total sum of all data in the running average set ****/
	s_dtaAccelDataX.Sum += s_dtaAccelDataX.CurrentData;
	s_dtaAccelDataY.Sum += s_dtaAccelDataY.CurrentData;
	s_dtaAccelDataZ.Sum += s_dtaAccelDataZ.CurrentData;

	/**** Calc Avg Values ****/
	s_dtaAccelDataX.Avg = (((float)s_dtaAccelDataX.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaAccelDataY.Avg = (((float)s_dtaAccelDataY.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaAccelDataZ.Avg = (((float)s_dtaAccelDataZ.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);

	/**** Set the initial Max and Min Values ****/
	if (s_dtaRunAvgCounter == 1) {
		s_dtaAccelDataX.Max = s_dtaAccelDataX.CurrentData;
		s_dtaAccelDataX.Min = s_dtaAccelDataX.CurrentData;
		s_dtaAccelDataY.Max = s_dtaAccelDataY.CurrentData;
		s_dtaAccelDataY.Min = s_dtaAccelDataY.CurrentData;
		s_dtaAccelDataZ.Max = s_dtaAccelDataZ.CurrentData;
		s_dtaAccelDataZ.Min = s_dtaAccelDataZ.CurrentData;
	}

	else {
	    /**** Calc Max Values ****/
	    if (s_dtaAccelDataX.CurrentData > s_dtaAccelDataX.Max) {
	    	s_dtaAccelDataX.Max = s_dtaAccelDataX.CurrentData;
	    }
	    if (s_dtaAccelDataY.CurrentData > s_dtaAccelDataY.Max) {
	    	s_dtaAccelDataY.Max = s_dtaAccelDataY.CurrentData;
	    }
	    if (s_dtaAccelDataZ.CurrentData > s_dtaAccelDataZ.Max) {
	    	s_dtaAccelDataZ.Max = s_dtaAccelDataZ.CurrentData;
	    }

	    /**** Calc Min Values ****/
	    if (s_dtaAccelDataX.CurrentData < s_dtaAccelDataX.Min) {
			s_dtaAccelDataX.Min = s_dtaAccelDataX.CurrentData;
		}
		if (s_dtaAccelDataY.CurrentData < s_dtaAccelDataY.Min) {
			s_dtaAccelDataY.Min = s_dtaAccelDataY.CurrentData;
		}
		if (s_dtaAccelDataZ.CurrentData < s_dtaAccelDataZ.Min) {
			s_dtaAccelDataZ.Min = s_dtaAccelDataZ.CurrentData;
		}
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaAvgGyro(Gyroscope_XyzData_T gyroData)
{
	/**** Insert Read Data into Data structure ****/
	s_dtaGyroDataX.CurrentData = gyroData.xAxisData;
	s_dtaGyroDataY.CurrentData = gyroData.yAxisData;
	s_dtaGyroDataZ.CurrentData = gyroData.zAxisData;

	/**** Calculate the total sum of all data in the running average set ****/
	s_dtaGyroDataX.Sum += s_dtaGyroDataX.CurrentData;
	s_dtaGyroDataY.Sum += s_dtaGyroDataY.CurrentData;
	s_dtaGyroDataZ.Sum += s_dtaGyroDataZ.CurrentData;

	/**** Calc Avg Values ****/
	s_dtaGyroDataX.Avg = (((float)s_dtaGyroDataX.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaGyroDataY.Avg = (((float)s_dtaGyroDataY.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaGyroDataZ.Avg = (((float)s_dtaGyroDataZ.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);

	/**** Set the initial Max and Min values ****/
	if (s_dtaRunAvgCounter == 1) {
		s_dtaGyroDataX.Max = s_dtaGyroDataX.CurrentData;
		s_dtaGyroDataX.Min = s_dtaGyroDataX.CurrentData;
		s_dtaGyroDataY.Max = s_dtaGyroDataY.CurrentData;
		s_dtaGyroDataY.Min = s_dtaGyroDataY.CurrentData;
		s_dtaGyroDataZ.Max = s_dtaGyroDataZ.CurrentData;
		s_dtaGyroDataZ.Min = s_dtaGyroDataZ.CurrentData;
	}

	else {
	    /**** Calc Max Values ****/
	    if (s_dtaGyroDataX.CurrentData > s_dtaGyroDataX.Max) {
	    	s_dtaGyroDataX.Max = s_dtaGyroDataX.CurrentData;
	    }
	    if (s_dtaGyroDataY.CurrentData > s_dtaGyroDataY.Max) {
	    	s_dtaGyroDataY.Max = s_dtaGyroDataY.CurrentData;
	    }
	    if (s_dtaGyroDataZ.CurrentData > s_dtaGyroDataZ.Max) {
	    	s_dtaGyroDataZ.Max = s_dtaGyroDataZ.CurrentData;
	    }

	    /**** Calc Min Values ****/
	    if (s_dtaGyroDataX.CurrentData < s_dtaGyroDataX.Min) {
			s_dtaGyroDataX.Min = s_dtaGyroDataX.CurrentData;
		}
		if (s_dtaGyroDataY.CurrentData < s_dtaGyroDataY.Min) {
			s_dtaGyroDataY.Min = s_dtaGyroDataY.CurrentData;
		}
		if (s_dtaGyroDataZ.CurrentData < s_dtaGyroDataZ.Min) {
			s_dtaGyroDataZ.Min = s_dtaGyroDataZ.CurrentData;
		}
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaAvgMag(Magnetometer_XyzData_T magData)
{
	/**** Insert Read Data into Data Structure ****/
	s_dtaMagDataX.CurrentData = magData.xAxisData;
	s_dtaMagDataY.CurrentData = magData.yAxisData;
	s_dtaMagDataZ.CurrentData = magData.zAxisData;

	/**** Calculate the total sum of all data in the running average set ****/
	s_dtaMagDataX.Sum += s_dtaMagDataX.CurrentData;
	s_dtaMagDataY.Sum += s_dtaMagDataY.CurrentData;
	s_dtaMagDataZ.Sum += s_dtaMagDataZ.CurrentData;

	/**** Calc Avg Values ****/
	s_dtaMagDataX.Avg = (((float)s_dtaMagDataX.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaMagDataY.Avg = (((float)s_dtaMagDataY.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaMagDataZ.Avg = (((float)s_dtaMagDataZ.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);

	/**** Set the initial Max and Min Values ****/
	if (s_dtaRunAvgCounter == 1) {
		s_dtaMagDataX.Max = s_dtaMagDataX.CurrentData;
		s_dtaMagDataX.Min = s_dtaMagDataX.CurrentData;
		s_dtaMagDataY.Max = s_dtaMagDataY.CurrentData;
		s_dtaMagDataY.Min = s_dtaMagDataY.CurrentData;
		s_dtaMagDataZ.Max = s_dtaMagDataZ.CurrentData;
		s_dtaMagDataZ.Min = s_dtaMagDataZ.CurrentData;
	}

	else {
	    /**** Calc Max Values ****/
	    if (s_dtaMagDataX.CurrentData > s_dtaMagDataX.Max) {
	    	s_dtaMagDataX.Max = s_dtaMagDataX.CurrentData;
	    }
	    if (s_dtaMagDataY.CurrentData > s_dtaMagDataY.Max) {
	    	s_dtaMagDataY.Max = s_dtaMagDataY.CurrentData;
	    }
	    if (s_dtaMagDataZ.CurrentData > s_dtaMagDataZ.Max) {
	    	s_dtaMagDataZ.Max = s_dtaMagDataZ.CurrentData;
	    }

	    /**** Calc Min Values ****/
	    if (s_dtaMagDataX.CurrentData < s_dtaMagDataX.Min) {
			s_dtaMagDataX.Min = s_dtaMagDataX.CurrentData;
		}
		if (s_dtaMagDataY.CurrentData < s_dtaMagDataY.Min) {
			s_dtaMagDataY.Min = s_dtaMagDataY.CurrentData;
		}
		if (s_dtaMagDataZ.CurrentData < s_dtaMagDataZ.Min) {
			s_dtaMagDataZ.Min = s_dtaMagDataZ.CurrentData;
		}
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaAvgLight(uint32_t lgtData)
{
	/**** Insert Read Data into Data Structure ****/
	s_dtaLightData.CurrentData = lgtData;

	/**** Calculate the total sum of all data in the running average set ****/
	s_dtaLightData.Sum += s_dtaLightData.CurrentData;

	/**** Calc Avg Values ****/
	s_dtaLightData.Avg = (((float)s_dtaLightData.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);

	/**** Set the initial Max and Min Values ****/
	if (s_dtaRunAvgCounter == 1) {
		s_dtaLightData.Max = s_dtaLightData.CurrentData;
		s_dtaLightData.Min = s_dtaLightData.CurrentData;
	}

	else {
	    /**** Calc Max Values ****/
	    if (s_dtaLightData.CurrentData > s_dtaLightData.Max) {
	    	s_dtaLightData.Max = s_dtaLightData.CurrentData;
	    }

	    /**** Calc Min Values ****/
	    if (s_dtaLightData.CurrentData < s_dtaLightData.Min) {
	    	s_dtaLightData.Min = s_dtaLightData.CurrentData;
		}
	}
}

/**
 * @brief in DTA_send_ch.h header
 */
static void dtaAvgEnv(Environmental_Data_T envData)
{
	/**** Insert Read Data into Data Stucture ****/
	s_dtaTempData.CurrentData = envData.temperature;
	s_dtaPressureData.CurrentData = envData.pressure;
	s_dtaHumidityData.CurrentData = envData.humidity;

	/**** Calculate the total sum of all data in the running average set ****/
	s_dtaTempData.Sum += s_dtaTempData.CurrentData;
	s_dtaPressureData.Sum += s_dtaPressureData.CurrentData;
	s_dtaHumidityData.Sum += s_dtaHumidityData.CurrentData;

	/**** Calc Avg Values ****/
	s_dtaTempData.Avg = (((float)s_dtaTempData.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaPressureData.Avg = (((float)s_dtaPressureData.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);
	s_dtaHumidityData.Avg = (((float)s_dtaHumidityData.Sum / (int32_t) s_dtaRunAvgCounter) + DTA_ROUNDING_VAR);

	/**** Set the initial Max and Min Values ****/
	if (s_dtaRunAvgCounter == 1) {
		s_dtaTempData.Max = s_dtaTempData.CurrentData;
		s_dtaTempData.Min = s_dtaTempData.CurrentData;
		s_dtaPressureData.Max = s_dtaPressureData.CurrentData;
		s_dtaPressureData.Min = s_dtaPressureData.CurrentData;
		s_dtaHumidityData.Max = s_dtaHumidityData.CurrentData;
		s_dtaHumidityData.Min = s_dtaHumidityData.CurrentData;
	}

	else {
	    /**** Calc Max Values ****/
	    if (s_dtaTempData.CurrentData > s_dtaTempData.Max) {
	    	s_dtaTempData.Max = s_dtaTempData.CurrentData;
	    }
	    if (s_dtaPressureData.CurrentData > s_dtaPressureData.Max) {
	    	s_dtaPressureData.Max = s_dtaPressureData.CurrentData;
	    }
	    if (s_dtaHumidityData.CurrentData > s_dtaHumidityData.Max) {
	    	s_dtaHumidityData.Max = s_dtaHumidityData.CurrentData;
	    }

	    /**** Calc Min Values ****/
	    if (s_dtaTempData.CurrentData < s_dtaTempData.Min) {
	    	s_dtaTempData.Min = s_dtaTempData.CurrentData;
		}
		if (s_dtaPressureData.CurrentData < s_dtaPressureData.Min) {
			s_dtaPressureData.Min = s_dtaPressureData.CurrentData;
		}
		if (s_dtaHumidityData.CurrentData < s_dtaHumidityData.Min) {
			s_dtaHumidityData.Min = s_dtaHumidityData.CurrentData;
		}
	}
}


/* global functions ********************************************************* */

/**
 * @brief in DTA_send_ih.h header
 */
void DTA_sendStreamDataTimerHandler(xTimerHandle pvParameters)
{
	/**** Initialize Variables ****/
	(void) pvParameters;
	Accelerometer_XyzData_T 	   _accelBMA280Data  = {CMN_NUMBER_UINT16_ZERO,CMN_NUMBER_UINT16_ZERO,CMN_NUMBER_UINT16_ZERO};
	Gyroscope_XyzData_T     	   _gyroBMG160Data	 = {CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO};
	Magnetometer_XyzData_T         _magData    	     = {CMN_NUMBER_UINT16_ZERO,CMN_NUMBER_UINT16_ZERO,CMN_NUMBER_UINT16_ZERO,CMN_NUMBER_UINT16_ZERO};
	Environmental_Data_T		   _envData		     = {CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO,CMN_NUMBER_UINT32_ZERO};
	uint32_t			           _lgtData		     = {CMN_NUMBER_UINT32_ZERO};

	/**** Read the Sensors ****/
	Accelerometer_readXyzGValue    (xdkAccelerometers_BMA280_Handle     , &_accelBMA280Data);
	Gyroscope_readXyzDegreeValue   (xdkGyroscope_BMG160_Handle 	    	, &_gyroBMG160Data);
	Environmental_readData         (xdkEnvironmental_BME280_Handle      , &_envData);
	LightSensor_readLuxData        (xdkLightSensor_MAX44009_Handle  	, &_lgtData);
	Magnetometer_readXyzTeslaData  (xdkMagnetometer_BMM150_Handle       , &_magData);

//#ifdef DTA_TEMP_OFFSET
//	/**** Temperature Offset ****/
//	_envData.temperature = dtaTempOffset(_envData.temperature);
//#endif

	/**** Average the Sensor Data ****/
	dtaAvgAccel(_accelBMA280Data);
	dtaAvgGyro(_gyroBMG160Data);
	dtaAvgMag(_magData);
	dtaAvgLight(_lgtData);
	dtaAvgEnv(_envData);

	/**** Increment the Running Avgerage Counter ****/
	s_dtaRunAvgCounter += 1;

	/**** Write the sensor data into the Stream Buffer in JSON Format ****/
	g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "{\n");
	g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"sn\": \"%s\",\n",  CFG_getMqttClientId());
	g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"data\": {\n");
	if (CFG_isAccelEnabled() == CFG_ENABLED) {
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"acc\": {\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"x\": %ld,\n", _accelBMA280Data.xAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"y\": %ld,\n", _accelBMA280Data.yAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"z\": %ld,\n", _accelBMA280Data.zAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"unit\": \"mG\"\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "},\n");
	}
	if (CFG_isGyroEnabled() == CFG_ENABLED) {
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"gyro\": {\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"x\": %ld,\n", _gyroBMG160Data.xAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"y\": %ld,\n", _gyroBMG160Data.yAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"z\": %ld,\n", _gyroBMG160Data.zAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"unit\": \"mdeg/s\"\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "},\n");
	}
	if (CFG_isMagEnabled() == CFG_ENABLED) {
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"mag\": {\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"x\": %ld,\n", _magData.xAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"y\": %ld,\n", _magData.yAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"z\": %ld,\n", _magData.zAxisData);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"unit\": \"uT\"\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "},\n");
	}
	if (CFG_isLightEnabled() == CFG_ENABLED) {
	    g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"light\": {\n");
	    g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"value\": %ld,\n", _lgtData);
	    g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"unit\": \"mLux\"\n");
	    g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "},\n");
	}
	if (CFG_isTempEnabled() == CFG_ENABLED) {
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"temp\": {\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"value\": %ld,\n", _envData.temperature);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"unit\": \"mCelsius\"\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "},\n");
	}
	if (CFG_isPresEnabled() == CFG_ENABLED) {
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"pressure\": {\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"value\": %ld,\n", _envData.pressure);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"unit\": \"Pascal\"\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "},\n");
	}
	if (CFG_isHumEnabled() == CFG_ENABLED) {
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"humidity\": {\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"value\": %ld,\n", _envData.humidity);
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "\"unit\": \"%%rh\"\n");
		g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + g_dtaStreamBuffer.length, "},\n");
	}
	g_dtaStreamBuffer.length += sprintf(g_dtaStreamBuffer.data + (g_dtaStreamBuffer.length - 2), "\n}\n}\n");
	g_dtaStreamBuffer.length -= 2;

	/**** Send the Report Data if the Correct Number of Samples have been taken ****/
	if (s_dtaRunAvgCounter == ((uint32_t) CFG_getReportSamples())) {
		DTA_sendReportData();
	}
}

/**
 * @brief in DTA_send_ih.h header
 */
void DTA_sendReportData(void)
{
	/**** Write the report data into the Report Buffer in JSON Format ****/
	g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "{\n");
	g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"sn\": \"%s\",\n",  CFG_getMqttClientId());
	g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"data\": {\n");
	if (CFG_isAccelEnabled() == CFG_ENABLED) {
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"acc\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"x\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaAccelDataX.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaAccelDataX.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaAccelDataX.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"y\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaAccelDataY.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaAccelDataY.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaAccelDataY.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"z\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaAccelDataZ.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaAccelDataZ.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaAccelDataZ.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"unit\": \"mG\"\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
	}
	if (CFG_isGyroEnabled() == CFG_ENABLED) {
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"gyro\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"x\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaGyroDataX.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaGyroDataX.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaGyroDataX.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"y\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaGyroDataY.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaGyroDataY.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaGyroDataY.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"z\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaGyroDataZ.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaGyroDataZ.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaGyroDataZ.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"unit\": \"mdeg/s\"\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
	}
	if (CFG_isMagEnabled() == CFG_ENABLED) {
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"mag\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"x\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaMagDataX.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaMagDataX.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaMagDataX.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"y\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaMagDataY.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaMagDataY.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaMagDataY.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"z\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaMagDataZ.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaMagDataZ.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld\n", s_dtaMagDataZ.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"unit\": \"uT\"\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
	}
	if (CFG_isLightEnabled() == CFG_ENABLED) {
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"light\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaLightData.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaLightData.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld,\n", s_dtaLightData.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"unit\": \"mLux\"\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
	}
	if (CFG_isTempEnabled() == CFG_ENABLED) {
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"temp\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaTempData.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaTempData.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld,\n", s_dtaTempData.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"unit\": \"mCelsius\"\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
	}
	if (CFG_isPresEnabled() == CFG_ENABLED) {
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"pressure\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaPressureData.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaPressureData.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld,\n", s_dtaPressureData.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"unit\": \"Pascal\"\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
	}
	if (CFG_isHumEnabled() == CFG_ENABLED) {
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"humidity\": {\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"min\": %ld,\n", s_dtaHumidityData.Min);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"max\": %ld,\n", s_dtaHumidityData.Max);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"avg\": %ld,\n", s_dtaHumidityData.Avg);
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "\"unit\": \"%%rh\"\n");
		g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + g_dtaReportBuffer.length, "},\n");
	}
	g_dtaReportBuffer.length += sprintf(g_dtaReportBuffer.data + (g_dtaReportBuffer.length - 2), "\n}\n}\n");
	g_dtaReportBuffer.length -= 2;

	/**** Reset the Averaging Data ****/
	DTA_resetReportData();
}

/**
 * @brief in DTA_send_ih.h header
 */
void DTA_sendConfigDataTimerHandler(xTimerHandle pvParameters)
{
	(void) pvParameters;

	/**** Write the configuration data into the Config Buffer in JSON Format ****/
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "{\n");
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"groupId\": \"%s\",\n",  CFG_getGroupId());
    g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"confRate\": %ld,\n", CFG_getConfigRate());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"reportSamples\": %ld,\n", CFG_getReportSamples());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"streamRate\": %ld,\n",CFG_getStreamRate());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"sensors\": {\n");
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"temp\": %s,\n", CFG_getTempStautsString());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"hum\": %s,\n", CFG_getHumStautsString());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"light\": %s,\n", CFG_getLightStautsString());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"pres\": %s,\n", CFG_getPresStautsString());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"acc\": %s,\n", CFG_getAccelStautsString());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"gyro\": %s,\n", CFG_getGyroStautsString());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "\"mag\": %s\n", CFG_getMagStautsString());
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "}\n");
	g_dtaConfigBuffer.length += sprintf(g_dtaConfigBuffer.data + g_dtaConfigBuffer.length, "}\n");
}

/**
 * @brief in DTA_send_ih.h header
 */
void DTA_resetReportData(void)
{
	/**** Reset the Report Data ****/
	s_dtaAccelDataX = ResetDataBuf;
	s_dtaAccelDataY = ResetDataBuf;
	s_dtaAccelDataZ = ResetDataBuf;
	s_dtaGyroDataX = ResetDataBuf;
	s_dtaGyroDataY = ResetDataBuf;
	s_dtaGyroDataZ = ResetDataBuf;
	s_dtaMagDataX = ResetDataBuf;
	s_dtaMagDataY = ResetDataBuf;
	s_dtaMagDataZ = ResetDataBuf;
	s_dtaLightData = ResetDataBuf;
	s_dtaTempData = ResetDataBuf;
	s_dtaPressureData = ResetDataBuf;
	s_dtaHumidityData = ResetDataBuf;

	s_dtaRunAvgCounter = DTA_INIT_ONE;
}

/**
 * @brief in DTA_send_ih.h header
 */
void DTA_init(void)
{
	/* Initialize all sensors */
	dtaInitializeAccel();
	dtaInitializeGyro();
	dtaInitializeMag();
	dtaInitializeLight();
	dtaInitializeEnv();

	/* Initialize Memory Buffers */
	pal_Memset(g_dtaStreamBuffer.data, 0x00, DTA_DATA_BUF_SIZE);
	g_dtaStreamBuffer.length = CMN_NUMBER_UINT32_ZERO;

	pal_Memset(g_dtaReportBuffer.data, 0x00, DTA_DATA_BUF_SIZE);
	g_dtaReportBuffer.length = CMN_NUMBER_UINT32_ZERO;

	pal_Memset(g_dtaConfigBuffer.data, 0x00, DTA_DATA_BUF_SIZE);
	g_dtaConfigBuffer.length = CMN_NUMBER_UINT32_ZERO;

	DTA_resetReportData();
}

/**
 * @brief in DTA_send_ih.h header
 */
void DTA_deinit(void)
{
  ;/* nothing to do */
}

/** ************************************************************************* */
