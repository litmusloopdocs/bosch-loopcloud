/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	DTA_send_ch.h
**
**	DESCRIPTION:	Local Header for DTA_send_cc.c source file
**
**	PURPOSE:        Contains the local macro, typedef, variables and function
**	                definitions for the DTA module
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2016.Jan         crk        BCDS         Consolidation/Cleanup of Code
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef DTA_SEND_CH_H_
#define DTA_SEND_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */
#define DTA_ROUNDING_VAR                        0.5
#define DTA_INIT_ONE                            1

// The temperature offset function is still being tested
// As every XDK will be placed in a different environment the results will vary.
// It is still best to compare the XDK temperature in the final environment
// to the known temperature in the environment (using another temperature measuring device)
// then by graphing the observed difference over time
// create an offset function to implement in the code.
// The provided Offset code was written with the default settings of the Bosch IoT Suite code
// i.e. all sensors enabled and streaming over wifi in 1s intervals.
//#warning The Temperature Offset Function is Still in the Early Beta Stages
////#define DTA_TEMP_OFFSET
//
//#ifdef DTA_TEMP_OFFSET
//typedef enum powerSource_e {
//	USB_POWER,
//	BATTERY
//} powerSource_t;
//
//#define DTA_POWER_SOURCE                        USB_POWER
//
///**** Temperature Offset Macros ****/
//// Use power equation f(x) = Cx^P to calculate temperature offset during "heat up" time
//#define DTA_OFFSET_COEFFICIENT                  1388.5    /**< Offset Coefficient (C) */
//#define DTA_OFFSET_POWER                        0.1762    /**< Offset Power Constant (P) */
//#define DTA_OFFSET_EQ_START_TIME                20        /**< Start of "heat up" time in seconds */
//#define DTA_OFFSET_EQ_END_TIME                  2000      /**< End of "heat up" time in seconds */
//#define DTA_OFFSET_PRE_EQ                       2000      /**< Offset used prior to "heat up" time in mC */
//#define DTA_OFFSET_POST_EQ                      5250      /**< Offset used after "heat up" time in mC */
//#define DTA_OFFSET_CHARGING                     2000      /**< Extra Offset if XDK is charging in mC*/
//
///**
// * @brief This functions offsets the read temperature value to more
// *        closely read out the actual temperature
// *
// * @param[in] Temperature - temperature data
// *
// * @return the new temperature value
// */
//static int32_t dtaTempOffset(int32_t temperature);
//#endif

// Report Data Structure
typedef struct {
	int32_t CurrentData;
	int32_t Avg;
	int32_t Max;
	int32_t Min;
	int32_t Sum;
} ReportData;

/* local module variable declarations */

// Data Variables
static uint32_t              s_dtaRunAvgCounter             = CMN_NUMBER_UINT32_ZERO;       /**< Running Average Counter */
static const ReportData      ResetDataBuf                   = { 0, 0, 0, 0, 0 };
static ReportData            s_dtaAccelDataX;
static ReportData            s_dtaAccelDataY;
static ReportData            s_dtaAccelDataZ;
static ReportData            s_dtaGyroDataX;
static ReportData            s_dtaGyroDataY;
static ReportData            s_dtaGyroDataZ;
static ReportData            s_dtaMagDataX;
static ReportData            s_dtaMagDataY;
static ReportData            s_dtaMagDataZ;
static ReportData            s_dtaLightData;
static ReportData            s_dtaTempData;
static ReportData            s_dtaPressureData;
static ReportData            s_dtaHumidityData;
//static uint8_t               s_dtaOffsetTimeFlag            = 0;

/* local module function declarations */

/**
 * @brief Inititialize the accelerometer
 */
static void dtaInitializeAccel(void);

/**
 * @brief Inititialize the gyroscope
 */
static void dtaInitializeGyro(void);

/**
 * @brief Inititialize the magnetometer
 */
static void dtaInitializeMag(void);

/**
 * @brief Inititialize the ambient light sensor
 */
static void dtaInitializeLight(void);

/**
 * @brief Inititialize the environmental sensor
 */
static void dtaInitializeEnv(void);

/**
 * @brief The function does the post initialisation of the light sensor
 */
static Retcode_T dtaPostInitLightSensor(void);

/**
 * @brief The function does the post initialisation of the magnetometer
 */
static Retcode_T dtaPostInitMagSensor(void);

/**
 * @Brief Setup the data in the data structure to report the
 *        Average, Max and Min values of the Accelerometer
 *
 * @param[in] accelData
 *            Current Data Read from the accelerometer
 */
static void dtaAvgAccel(Accelerometer_XyzData_T accelData);

/**
 * @Brief Setup the data in the data structure to report the
 *        Average, Max and Min values of the Gyroscope
 *
 * @param[in] gyroData
 *            Current Data Read from the gyroscope
 */
static void dtaAvgGyro(Gyroscope_XyzData_T gyroData);

/**
 * @Brief Setup the data in the data structure to report the
 *        Average, Max and Min values of the Magnetometer
 *
 * @param[in] magData
 *            Current Data Read from the magnetometer
 */
static void dtaAvgMag(Magnetometer_XyzData_T magData);

/**
 * @Brief Setup the data in the data structure to report the
 *        Average, Max and Min values of the ambient light
 *
 * @param[in] lgtData
 *            Current Data Read from the ambient light sensor
 */
static void dtaAvgLight(uint32_t lgtData);

/**
 * @Brief Setup the data in the data structure to report the
 *        Average, Max and Min values of the Temperature,
 *        Humidity and Pressure
 *
 * @param[in] envData
 *            Current Data Read from the environmental sensor
 */
static void dtaAvgEnv(Environmental_Data_T envData);

/* local inline function definitions */

#endif /* DTA_SEND_CH_H_ */

/** ************************************************************************* */
