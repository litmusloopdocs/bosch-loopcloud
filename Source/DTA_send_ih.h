/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	DTA_send_ih.h
**
**	DESCRIPTION:	Public Interface Header for DTA_send_cc.c source file
**
**	PURPOSE:        Contains the public macro, typedef, variables and function
**	                definitions for the DTA module
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2016.Jan         crk        BCDS         Consolidation/Cleanup of Code
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef DTA_SEND_IH_H_
#define DTA_SEND_IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */
#define DTA_DATA_BUF_SIZE    1024

typedef struct {
	uint32_t length;
	char data[DTA_DATA_BUF_SIZE];
} DataBuffer;

/* public function prototype declarations */

/**
 * @brief The function to get and print the sensors streaming data
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void DTA_sendStreamDataTimerHandler(xTimerHandle pvParameters);

/**
 * @brief The function to print the sensors report data
 */
void DTA_sendReportData(void);

/**
 * @brief The function to print the configuration data
 *
 * @param[in] pvParameters Rtos task should be defined with the type void *(as argument)
 */
void DTA_sendConfigDataTimerHandler(xTimerHandle pvParameters);

/**
 * @brief Reset the Report Data for all Sensors
 */
void DTA_resetReportData(void);

/**
 * @brief Init the DTA module
 */
void DTA_init(void);

/**
 * @brief Uninit the DTA module
 */
void DTA_deinit(void);

/* public global variable declarations */

// Data Buffers
extern DataBuffer g_dtaStreamBuffer;
extern DataBuffer g_dtaReportBuffer;
extern DataBuffer g_dtaConfigBuffer;

/* inline function definitions */

#endif /* DTA_SEND_IH_H_ */

/** ************************************************************************* */
