/*
  * mqtt_client.c - Sample application to connect to a MQTT broker and
 * exercise functionalities like subscribe, publish etc.
 *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 * All rights reserved. Property of Texas Instruments Incorporated.
 * Restricted rights to use, duplicate or disclose this code are
 * granted through contract.
 *
 * The program may not be used without the written permission of
 * Texas Instruments Incorporated or against the terms and conditions
 * stipulated in the agreement under which this program has been supplied,
 * and under no circumstances can it be used with non-TI connectivity device.
 *
 *
 * Application Name     -   MQTT Client
 * Application Overview -   This application acts as a MQTT client and connects
 *							to the IBM MQTT broker, simultaneously we can
 *							connect a web client from a web browser. Both
 *							clients can inter-communicate using appropriate
 *						    topic names.
 *
 * Application Details  -   http://processors.wiki.ti.com/index.php/CC31xx_MQTT_Client
 *                          docs\examples\mqtt_client.pdf
 *
 */

/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	LOOPMQ_Client_cc.c
**
**	DESCRIPTION:	Source Code for the MQTT Client
**
**	PURPOSE:        Contains all the definitions and functions to Initialize the
**	                MQTT Client, connect to the Bosch IoT MQTT broker,
**	                subscribe and publish to the MQTT broker, set up timers and the
**	                main MQTT task to publish the sensor data from the XDK.
**
**	AUTHOR(S):		Texas Instruments (info listed above)
**	                Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2014             unknown    TI           Initial Release (see above)
**  2015.Oct         crk        BCDS         Edited the TI code to work with the
**                                           Bosch IoT Cloud Platform updating callbacks,
**                                           Handling Timers, publishing XDK Data,
**                                           changing subscriptions, etc
** 2016.Feb          crk        BCDS         Finished updating MQTT client for
**                                           Bosch Iot Cloud and general cleanup
**
**
*******************************************************************************/

/* system header files */
#include <stdint.h>
#include <stdlib.h>

/* interface header files */
#include "FreeRTOS.h"
#include "timers.h"
#include "task.h"
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "em_gpio.h"

/* other header files */
#include "sl_common.h"
#include "sl_mqtt_client.h"

/* own header files */
#include "CMN_common_ch.h"
#include "DTA_send_ih.h"
#include "CFG_parser_ih.h"
#include "WNS_com_ih.h"
#include "LOOPMQ_Client_ch.h"
#include "LOOPMQ_Client_ih.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* Connection configuration */
connect_config mqttConnectConfig =
{
	{
		{
			SL_MQTT_NETCONN_URL,
			CFG_DEFAULT_MQTT_BROKER_NAME,
			CFG_DEFAULT_MQTT_PORT,
			0,
			0,
			0,
			NULL
		},
		SERVER_MODE,
		true,
	},
	NULL,
	(_u8*) WNS_DEFAULT_MAC, /* Must be unique */
	NULL,
	NULL,
	true,
	KEEP_ALIVE_TIMER,
	{mqttRecv, mqttEvt, mqttDisconnect},
	SUB_TOPIC_COUNT,
	{SUB_TOPIC1, SUB_TOPIC2},
	{QOS2, QOS2},
	{WILL_TOPIC, WILL_MSG, WILL_QOS, WILL_RETAIN},
	false
};

/* library configuration */
SlMqttClientLibCfg_t mqttClientConfig =
{
    LOOPBACK_PORT_NUMBER,
    MQTT_CLIENT_TASK_PRIORITY,
    RCV_TIMEOUT,
    true,
    (_i32(*)(_const char *, ...))mqttDummy
};

/* MQTT Configuration Data */
void *mqttApplicationHndl = &mqttConnectConfig;

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static _i32 mqttDummy(_const char *inBuff, ...)
{
	(void) inBuff;
    return 0;
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttStopTimer(xTimerHandle TimerHandle) {
	/* Stop the timer */
	xTimerStop(TimerHandle, UINT32_MAX);
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttUpdateTimerPeriod(void)
{
	/**** Initialize Variables ****/
	int8_t _err = CMN_FALSE;

	/**** Read the current Streaming Rate ****/
	s_mqttTimerStreamValue = CFG_getStreamRate();
	s_mqttTimerConfigValue = CFG_getConfigRate();

	/**** Update the timer periods if valid, else stop the timers ****/
	if(CMN_FASTEST_TRANSMISSION_SPEED <= s_mqttTimerStreamValue ) {
		_err = xTimerChangePeriod(s_mqttTimerStreamHandler, s_mqttTimerStreamValue/portTICK_PERIOD_MS, 100);
		/* Timer start fail case */
		if (pdFAIL == _err) {
			printf("The Stream timer was not started, Due to Insufficient heap memory\r\n");
		}
	}
	else {
		printf("Invalid stream rate = %ld; Turn Off Timer \r\n", s_mqttTimerStreamValue);
		s_mqttTimerStreamValue = CMN_NUMBER_UINT32_ZERO;
		_err = xTimerStop(s_mqttTimerStreamHandler, UINT32_MAX);
	}

	if(CMN_FASTEST_TRANSMISSION_SPEED <= s_mqttTimerConfigValue ) {
		_err = xTimerChangePeriod(s_mqttTimerConfigHandler, s_mqttTimerConfigValue/portTICK_PERIOD_MS, 100);
		/* Timer start fail case */
		if (pdFAIL == _err) {
			printf("The Config timer was not started, Due to Insufficient heap memory\r\n");
		}
	}
	else {
		printf("Invalid config rate = %ld; Turn Off Timer \r\n", s_mqttTimerConfigValue);
		s_mqttTimerConfigValue = CMN_NUMBER_UINT32_ZERO;
		_err = xTimerStop(s_mqttTimerConfigHandler, UINT32_MAX);
	}
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static int mqttClientWrite(unsigned char *inBuff, int bufsz)
{
	/**** Initialize Variables ****/
	int _res = CMN_FALSE;

	/**** Send the buffer data ****/
    _res = sl_Send(s_mqttWifiConnectionHandler, inBuff, bufsz, 0 /* flags not used*/);

    /**** Check for Errors ****/
	if(-1 == _res ){
		printf("sl_Send could not write \r\n");
	}

	return _res;
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttRecv(void *application_hndl, _const char *topstr, _i32 top_len,
		             _const void *payload, _i32 pay_len, bool dup,_u8 qos, bool retain)
{
	/**** Initialize Variables ****/
	(void) application_hndl;
    _i8 *_outputStr=(_i8*)malloc(top_len+1);
    pal_Memset(_outputStr,'\0',top_len+1);
    pal_Strncpy(_outputStr, topstr, top_len);
    _outputStr[top_len]='\0';
    int _configFlag = CMN_FALSE;

    /**** Compare the received topic with the subscribed topics ****/
    if((pal_Strncmp(_outputStr, mqttSubTopicSingle_ptr, top_len) == 0) || (pal_Strncmp(_outputStr, mqttSubTopicGroup_ptr, top_len) == 0))
    {
    	/* Turn Yellow LED On During Configuration */
    	PTD_pinOutSet(PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);
    	PTD_pinOutClear(PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);
    	PTD_pinOutClear(PTD_PORT_LED_RED, PTD_PIN_LED_RED);

        /* Stop Timers and Task During Configuration */
        xTimerStop(s_mqttTimerStreamHandler, UINT32_MAX);
        xTimerStop(s_mqttTimerConfigHandler, UINT32_MAX);
        vTaskSuspend(s_mqttTaskClientHandler);

    	_configFlag = CMN_TRUE;
    }

    /**** Publish Received Topic to the MQTT Broker ****/
    mqttClientWrite((_u8 *)"\n\r Publish Message Received",27);
    
    pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
    sprintf((char*) mqttPrintBuf, "\n\r Topic: %s", _outputStr);
    mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

    free(_outputStr);

    /**** Publish QoS to the MQTT Broker ****/
    pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
    sprintf((char*) mqttPrintBuf, " [Qos: %d] ", qos);
    mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

    if(retain)
      mqttClientWrite((_u8 *)" [Retained]", 11);
    if(dup)
      mqttClientWrite((_u8 *)" [Duplicate]", 12);
    
    /**** Reset the Output String to point to the receieved data payload  ****/
    _outputStr=(_i8*)malloc(pay_len+1);
    pal_Memset(_outputStr,'\0',pay_len+1);
    pal_Strncpy(_outputStr, payload, pay_len);
    _outputStr[pay_len]='\0';

    /**** Publish the receieved data payload to the MQTT Broker  ****/
    pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
    sprintf((char*) mqttPrintBuf, " \n\r Data is: %s\n\r", (char*)_outputStr);
    mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

    if (_configFlag) {
        /* Parse the JSON File Received */
        if(CFG_ParseJsonConfigFile((const char *)_outputStr, pay_len) != CMN_TRUE) {
        	printf("Error Parsing MQTT Configuration File");
        }

        /**** Update the Client's Configuration ****/
        mqttUpdateTimerPeriod();
        mqttQueue = SUBCRIBE_UPDATE;

        /* Restart the Timers and Task for Data Transfer */
        xTimerStart(s_mqttTimerStreamHandler, 100);
        xTimerStart(s_mqttTimerConfigHandler, 100);
        vTaskResume(s_mqttTaskClientHandler);

    	/* Turn Orange LED On to indicate Data Aquisition */
    	PTD_pinOutSet(PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);
    	PTD_pinOutClear(PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);
    	PTD_pinOutClear(PTD_PORT_LED_RED, PTD_PIN_LED_RED);
    }

    free(_outputStr);
    return;
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttEvt(void *application_hndl, _i32 evt, _const void *buf, _u32 len)
{
	/**** Initialize Variables ****/
	(void) application_hndl;
    _u32 _i;
    
    /**** MQTT Event State Machine ****/
    switch(evt)
    {
        /**** Publish Acknowledge Event ****/
        case SL_MQTT_CL_EVT_PUBACK:
        {
            mqttClientWrite((_u8 *)" PubAck:\n\r", 10);
        }
        break;

        /**** Subscribe Acknowledge Event ****/
        case SL_MQTT_CL_EVT_SUBACK:
        {
            mqttClientWrite((_u8 *)"\n\r Granted QoS Levels\n\r", 23);
        
            for(_i = 0; _i < len; _i++)
            {
                pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
                sprintf((char*) mqttPrintBuf, " QoS %d\n\r",((_u8*)buf)[_i]);
                mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);
            }
        }
        break;

        /**** Unsubscribe Acknowledge Event ****/
        case SL_MQTT_CL_EVT_UNSUBACK:
        {
            mqttClientWrite((_u8 *)" UnSub Ack:\n\r", 13);
        }
        break;
    
        /**** Unexpected Event ****/
        default:
        {
            mqttClientWrite((_u8 *)" [MQTT EVENT] Unexpected event \n\r", 33);
        }
        break;
    }
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttDisconnect(void *application_hndl)
{
	/**** Initialize Variables ****/
    connect_config *_localConnConf;
    mqttQueue = BROKER_DISCONNECTION;
    _localConnConf = application_hndl;

    /**** Unsubscribe to Topics ****/
    sl_ExtLib_MqttClientUnsub(_localConnConf->clt_ctx, _localConnConf->topic,
                              SUB_TOPIC_COUNT);

    /**** Write the Print Buffer to the WiFi handler ****/
    pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
    sprintf((char*) mqttPrintBuf, " Disconnect from broker %s\r\n",
           (_localConnConf->broker_config).server_info.server_addr);
    mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

    /**** Delete the Client Context Information ****/
    _localConnConf->is_connected = false;
    sl_ExtLib_MqttClientCtxDelete(_localConnConf->clt_ctx);
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void publishData(void *clt_ctx, _const char *publish_topic, _u8 *publish_data)
{
	/**** Initialize Variables ****/
    _i32 retVal = -1;

    /**** Send the publish message ****/
    retVal = sl_ExtLib_MqttClientSend((void*)clt_ctx,
            publish_topic ,publish_data, pal_Strlen(publish_data), QOS2, RETAIN);
    if(retVal < 0) {
        mqttClientWrite((_u8 *)"\n\r CC3100 failed to publish the message\n\r", 41);
        return;
    }

    /**** Write Topic to the MQTT Server ****/
    mqttClientWrite((_u8 *)"\n\r CC3100 Publishes the following message \n\r", 44);
    pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
    sprintf((char*) mqttPrintBuf, " Topic: %s\n\r", publish_topic);
    mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

    /**** Write JSON Data to MQTT Server ****/
   	mqttClientWrite((_u8 *) publish_data, DTA_DATA_BUF_SIZE);
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttClientUnSubscribe(void)
{
	/**** Initialize Variables ****/
	connect_config *_localConnConf = (connect_config *)mqttApplicationHndl;

    /**** Unsubscribe to all currently subscribed topics ****/
    if(sl_ExtLib_MqttClientUnsub((void*)_localConnConf->clt_ctx,
							   _localConnConf->topic, SUB_TOPIC_COUNT) < 0)
    {
    	/**** Error Occured, let WiFi handler know ****/
        pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
        sprintf((char*) mqttPrintBuf, "\n\r Un-Subscribe Error \n\r");
        mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

        /*** Disconnect from the MQTT Broker ****/
        mqttClientWrite((_u8 *)"Disconnecting from the broker\r\n", 31);
        sl_ExtLib_MqttClientDisconnect(_localConnConf->clt_ctx);
		_localConnConf->is_connected = false;

        /**** Delete the context for this connection ****/
        sl_ExtLib_MqttClientCtxDelete(_localConnConf->clt_ctx);
    }
    else
    {
        _i32 iSub;

        /**** Publish to MQTT Broker Each Newly Unsubscribed Topics ****/
        mqttClientWrite((_u8 *)"Client un-subscribed on following topics:\n\r", 43);
        for(iSub = 0; iSub < _localConnConf->num_topics; iSub++)
        {
            pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
            sprintf((char*) mqttPrintBuf, " %s\n\r", _localConnConf->topic[iSub]);
            mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);
        }
    }
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttClientSubscribe(void)
{
	/**** Intialize Variables ****/
	connect_config *_localConnConf = (connect_config *)mqttApplicationHndl;

	/**** Set Subscribe Topic Strings ****/
	pal_Memset(mqttSubTopicSingle, 0x00, MQTT_PRINT_BUF_LEN);
	sprintf((char*) mqttSubTopicSingle, SUB_TOPIC1, (const char*) CFG_getMqttClientId());
	mqttSubTopicSingle_ptr = (char*) mqttSubTopicSingle;
	_localConnConf->topic[SUB_TOPIC_1] = mqttSubTopicSingle;

	pal_Memset(mqttSubTppicGroup, 0x00, MQTT_PRINT_BUF_LEN);
	sprintf((char*) mqttSubTppicGroup, SUB_TOPIC2, (const char*) CFG_getGroupId());
	mqttSubTopicGroup_ptr = (char*) mqttSubTppicGroup;
	_localConnConf->topic[SUB_TOPIC_2] = mqttSubTppicGroup;

    /**** Subscribe to topics ****/
    if(sl_ExtLib_MqttClientSub((void*)_localConnConf->clt_ctx,
							   _localConnConf->topic,
							   _localConnConf->qos, SUB_TOPIC_COUNT) < 0)
    {
    	/**** Subscribe Error, let the WiFi Handler Know ****/
        pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
        sprintf((char*) mqttPrintBuf, "\n\r Subscription Error \n\r");
        mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

        /**** Disconnect from the MQTT Broker ****/
        mqttClientWrite((_u8 *)"Disconnecting from the broker\r\n", 31);
        sl_ExtLib_MqttClientDisconnect(_localConnConf->clt_ctx);
		_localConnConf->is_connected = false;

        /**** Delete the context for this connection ****/
        sl_ExtLib_MqttClientCtxDelete(_localConnConf->clt_ctx);
    }
    else
    {
        _i32 iSub;

        /**** Publish to MQTT Broker Each Newly Subscribed Topics ****/
        mqttClientWrite((_u8 *)"Client subscribed on following topics:\n\r", 40);
        for(iSub = 0; iSub < _localConnConf->num_topics; iSub++)
        {
            pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
            sprintf((char*) mqttPrintBuf, " %s\n\r", _localConnConf->topic[iSub]);
            mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);
        }
    }
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttClientInit(void)
{
	/**** Initialize Variables ****/
    _i32 retVal = -1;
    connect_config *_localConnConf = (connect_config *)mqttApplicationHndl;

    _localConnConf->client_id = (_u8*) CFG_getMqttClientId();
    _localConnConf->broker_config.server_info.server_addr = (const char*) CFG_getMqttBrokerName();
    _localConnConf->broker_config.server_info.port_number = (_u16) CFG_getMqttPort();

    /**** Initialize MQTT client lib ****/
    retVal = sl_ExtLib_MqttClientInit(&mqttClientConfig);
    if(retVal != 0) {
        /**** Library initialization failed ****/
    	mqttClientWrite((_u8 *)"MQTT Client lib initialization failed\n\r", 39);
    	mqttClientExit();
    }

	/**** create client context ****/
	_localConnConf->clt_ctx =
	sl_ExtLib_MqttClientCtxCreate(&_localConnConf->broker_config,
								  &_localConnConf->CallBAcks,
								  &(_localConnConf));

	/**** Set Client ID ****/
	sl_ExtLib_MqttClientSet((void*)_localConnConf->clt_ctx,
							SL_MQTT_PARAM_CLIENT_ID,
							_localConnConf->client_id,
							pal_Strlen((_localConnConf->client_id)));

	/**** Set will Params ****/
	if(_localConnConf->will_params.will_topic != NULL)
	{
		sl_ExtLib_MqttClientSet((void*)_localConnConf->clt_ctx,
								SL_MQTT_PARAM_WILL_PARAM,
								&(_localConnConf->will_params),
								sizeof(SlMqttWill_t));
	}

	/**** Setting user name and password ****/
	if(_localConnConf->usr_name != NULL)
	{
		sl_ExtLib_MqttClientSet((void*)_localConnConf->clt_ctx,
								SL_MQTT_PARAM_USER_NAME,
								_localConnConf->usr_name,
								pal_Strlen(_localConnConf->usr_name));

		if(_localConnConf->usr_pwd != NULL)
		{
			sl_ExtLib_MqttClientSet((void*)_localConnConf->clt_ctx,
									SL_MQTT_PARAM_PASS_WORD,
									_localConnConf->usr_pwd,
									pal_Strlen(_localConnConf->usr_pwd));
		}
	}

	/**** Delay added to allow setup time ****/
	vTaskDelay(2000/portTICK_PERIOD_MS);

	/**** Connecting to the broker ****/
	if((sl_ExtLib_MqttClientConnect((void*)_localConnConf->clt_ctx,
									_localConnConf->is_clean,
									_localConnConf->keep_alive_time) & 0xFF) != 0)
	{
		/**** Error Connecting to the MQTT Broker, Infom WiFI Handler ****/
		pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
		sprintf((char*) mqttPrintBuf, "\n\r Broker connect failed \n\r");
		mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

		/**** Delete the context for this connection ****/
		sl_ExtLib_MqttClientCtxDelete(_localConnConf->clt_ctx);
		mqttClientExit();
	}
	else {
		/**** Connection to the Broker was Successful ****/
		pal_Memset(mqttPrintBuf, 0x00, MQTT_PRINT_BUF_LEN);
		sprintf((char*) mqttPrintBuf, "\n\r Success: conn to Broker \n\r ");
		mqttClientWrite((_u8 *) mqttPrintBuf, MQTT_PRINT_BUF_LEN);

		_localConnConf->is_connected = true;
	}

	/**** Subscribe to Topics ****/
	mqttClientSubscribe();

    /**** Set Public Topics ****/
	pal_Memset(mqttPubTopicStream, 0x00, MQTT_PRINT_BUF_LEN);
	sprintf((char*) mqttPubTopicStream, PUB_TOPIC_1, (const char*) CFG_getMqttClientId());
	mqttPubTopicStream_ptr = (char*) mqttPubTopicStream;

	pal_Memset(mqttPubTopicReport, 0x00, MQTT_PRINT_BUF_LEN);
	sprintf((char*) mqttPubTopicReport, PUB_TOPIC_2, (const char*) CFG_getMqttClientId());
	mqttPubTopicReport_ptr = (char*) mqttPubTopicReport;

	pal_Memset(mqttPubTopicConfig, 0x00, MQTT_PRINT_BUF_LEN);
	sprintf((char*) mqttPubTopicConfig, PUB_TOPIC_3, (const char*) CFG_getMqttClientId());
	mqttPubTopicConfig_ptr = (char*) mqttPubTopicConfig;

	/**** Turn Orange LED On After Connection to MQTT Broker ****/
	PTD_pinOutSet(PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);
	PTD_pinOutClear(PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);
	PTD_pinOutClear(PTD_PORT_LED_RED, PTD_PIN_LED_RED);
    return;
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttClientTask(void *pvParameters)
{
	/**** Initialize Variables ****/
	(void) pvParameters;
	connect_config *_localConnConf = (connect_config *)mqttApplicationHndl;

	/**** Forever Loop Necessary for freeRTOS Task ****/
    for(;;)
    {
    	/**** Publish Live Data Stream ****/
        if(g_dtaStreamBuffer.length > CMN_NUMBER_UINT32_ZERO)
        {
        	publishData((void*)_localConnConf->clt_ctx, mqttPubTopicStream_ptr, (_u8*) g_dtaStreamBuffer.data);

        	pal_Memset(g_dtaStreamBuffer.data, 0x00, DTA_DATA_BUF_SIZE);
        	g_dtaStreamBuffer.length = CMN_NUMBER_UINT32_ZERO;
        }
        /**** Publish Report Data Stream ****/
        else if(g_dtaReportBuffer.length > CMN_NUMBER_UINT32_ZERO)
		{
			publishData((void*)_localConnConf->clt_ctx, mqttPubTopicReport_ptr, (_u8*) g_dtaReportBuffer.data);

        	pal_Memset(g_dtaReportBuffer.data, 0x00, DTA_DATA_BUF_SIZE);
        	g_dtaReportBuffer.length = CMN_NUMBER_UINT32_ZERO;
		}
        /**** Publish Configuration Data Stream ****/
        else if(g_dtaConfigBuffer.length > CMN_NUMBER_UINT32_ZERO)
		{
			publishData((void*)_localConnConf->clt_ctx, mqttPubTopicConfig_ptr, (_u8*) g_dtaConfigBuffer.data);

        	pal_Memset(g_dtaConfigBuffer.data, 0x00, DTA_DATA_BUF_SIZE);
        	g_dtaConfigBuffer.length = CMN_NUMBER_UINT32_ZERO;
		}
        /**** Subscribe to New Topics ****/
        else if(SUBCRIBE_UPDATE == mqttQueue) {
            mqttClientUnSubscribe();
            mqttClientSubscribe();
            mqttQueue = NO_ACTION;
        }
        /**** Disconnect Form the MQTT Broker ****/
        else if(BROKER_DISCONNECTION == mqttQueue)
        {
            mqttQueue = NO_ACTION;
            mqttClientExit();
        }
    }
}

/**
 * @brief in LOOPMQ_Client_ch.h header
 */
static void mqttClientExit(void)
{
	/**** De-initializing the client library ****/
	sl_ExtLib_MqttClientExit();
	mqttClientWrite((_u8 *)"\n\r Exiting the Application\n\r", 28);

	/* Turn Red LED On If Failure to Connect to Client */
	PTD_pinOutSet(PTD_PORT_LED_RED, PTD_PIN_LED_RED);
	PTD_pinOutClear(PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);
	PTD_pinOutClear(PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);

	/**** Error Forever Loop ****/
	LOOP_FOREVER();
}

/* global functions ********************************************************* */

/**** Functions Inherent to the TI Code, Never teseted Removal ****/
/**** Asychronous Event Handlers ****/
/**
 * @brief in LOOPMQ_Client_ih.h header
 */
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent)
{
	(void) pDevEvent;
    /**** Most of the general errors are not FATAL are are to be handled
          appropriately by the application ****/
    mqttClientWrite((_u8 *)" [GENERAL EVENT] \n\r", 19);
}

/**
 * @brief in LOOPMQ_Client_ih.h header
 */
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock)
{
	(void) pSock;
    /*
     * This application doesn't work w/ socket - Hence not handling these events
     */
    mqttClientWrite((_u8 *)" [SOCK EVENT] Unexpected event \n\r", 33);
}

/**** FreeRTOS User Hook Functions enabled in FreeRTOSConfig.h ****/

/**
 * @brief in LOOPMQ_Client_ih.h header
 */
void vAssertCalled(_const _i8 *pcFile, _u32 ulLine)
{
	(void) pcFile;
	(void) ulLine;
    /* Handle Assert here */
    while(1)
    {
    }
}

/**
 * @brief in LOOPMQ_Client_ih.h header
 */
void vApplicationIdleHook(void)
{
    /* Handle Idle Hook for Profiling, Power Management etc */
}

/**
 * @brief in LOOPMQ_Client_ih.h header
 */
void vApplicationMallocFailedHook(void)
{
    /* Handle Memory Allocation Errors */
    while(1)
    {
    }
}

/**
 * @brief in LOOPMQ_Client_ih.h header
 */
void MQTT_setWifiConnectionHandler(int16_t tcpSocket, int8_t flag)
{
	/**** Set the wifi Connection Handler ****/
	s_mqttWifiConnectionHandler = tcpSocket;

	/**** Error Stop Timers ****/
	if (CMN_TRUE != flag) {
		mqttStopTimer(s_mqttTimerStreamHandler);
		mqttStopTimer(s_mqttTimerConfigHandler);
	}
}


/**
 * @brief in LOOPMQ_Client_ih.h header
 */
void MQTT_init(void)
{
	/**** Initialize Variables ****/
    int retVal;
	long _StreamID = 111;
	long _ConfigID = 112;

	/**** Initialize the MQTT Client ****/
    mqttClientInit();

	/**** Create Live Data Stream Timer ****/
	s_mqttTimerStreamHandler = xTimerCreate(
			(const char * const) "StreamDataTimer",
			CFG_getStreamRate()/portTICK_RATE_MS,
			CMN_TIMER_AUTO_RELOAD_ON,
			(void *)_StreamID,
			DTA_sendStreamDataTimerHandler);

	/**** Create Configuration Data Stream Timer ****/
	s_mqttTimerConfigHandler = xTimerCreate(
			(const char * const) "ConfigDataTimer",
			CFG_getConfigRate()/portTICK_RATE_MS,
			CMN_TIMER_AUTO_RELOAD_ON,
			(void *)_ConfigID,
			DTA_sendConfigDataTimerHandler);

	/**** Create MQTT Client Task ****/
    retVal = xTaskCreate(mqttClientTask, (const char * const) "Mqtt Client App",
                    		MQTT_TASK_STACK_SIZE, NULL, MQTT_TASK_PRIORITY, &s_mqttTaskClientHandler);

    /**** Start the Data Timers ****/
    xTimerStart(s_mqttTimerStreamHandler, 100/portTICK_RATE_MS);
    xTimerStart(s_mqttTimerConfigHandler, 100/portTICK_RATE_MS);

    /**** Error Occured Exit App ****/
    if(retVal < 0)
    {
    	mqttClientExit();
    }
    return;
}
