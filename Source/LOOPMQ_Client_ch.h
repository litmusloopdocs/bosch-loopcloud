/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	LOOPMQ_Client_ch.h
**
**	DESCRIPTION:	Local Header for CFG_parser_cc.c source file
**
**	PURPOSE:        Contains the local macro, typedef, variables and function
**	                definitions for the CFG module
**
**	AUTHOR(S):		Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**  2016.Jan         crk        BCDS         Initial Version
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef LOOPMQ_CLIENT_CH_H_
#define LOOPMQ_CLIENT_CH_H_

/* local interface declaration ********************************************** */

/* local type and macro definitions */

//MQTT Configuration Macros
#define MQTT_3_1_1              false /*MQTT 3.1.1 */
#define MQTT_3_1                true  /*MQTT 3.1 */
#define SERVER_MODE             MQTT_3_1

// MQTT Will Configurations
#define WILL_TOPIC              "Client"
#define WILL_MSG                "Client Stopped"
#define WILL_QOS                QOS2
#define WILL_RETAIN             false

/*Specifying Receive time out for the Receive task*/
#define RCV_TIMEOUT             30

/* Keep Alive Timer value in seconds.
 * Maximum time interval between messages received from a client.
 */
#define KEEP_ALIVE_TIMER        25

/* Clean session flag. The server must discard any previously
 * maintained information about the client.
 */
#define CLEAN_SESSION           true

/* Retain Flag. Used in publish message.
 * The server should hold on to the publish message after it has
 * been delivered to the current subscribers.
 */
#define RETAIN                  1

/* Defining Publish Topics */
//#define PUB_TOPIC_1             "BCDS/XDK/single/%s/out/stream"
//#define PUB_TOPIC_2             "BCDS/XDK/single/%s/out/report"
//#define PUB_TOPIC_3             "BCDS/XDK/single/%s/out/config"

/* Defining Publish Topics */
#define PUB_TOPIC_1             "loop1"
#define PUB_TOPIC_2             "loop2"
#define PUB_TOPIC_3             "loop3"

/* Defining Subscription Topics */
//#define SUB_TOPIC1              "BCDS/XDK/single/%s/in/config"
//#define SUB_TOPIC2              "BCDS/XDK/group/%s/in/config"

/* Defining Subscription Topics */
#define SUB_TOPIC1              "loop4"
#define SUB_TOPIC2              "loop5"

/* Defining QOS levels */
#define QOS0                    0
#define QOS1                    1
#define QOS2                    2

/* Task priorities and OSI Stack Size */
#define MQTT_TASK_STACK_SIZE            1024
#define MQTT_TASK_PRIORITY              1
#define MQTT_CLIENT_TASK_PRIORITY       3

#define LOOPBACK_PORT_NUMBER    1882

#define MQTT_PRINT_BUF_LEN      512

#undef  LOOP_FOREVER
#define LOOP_FOREVER() \
            {\
                while(1) \
                { \
                    vTaskDelay(10/portTICK_PERIOD_MS); \
                } \
            }

// Subscription Topic Count Index
typedef enum {
    SUB_TOPIC_1 ,
	SUB_TOPIC_2 ,
	SUB_TOPIC_COUNT ,
} MQTT_SubTopics;

// MQTT Connection Configuration Structure
typedef struct connection_config
{
    SlMqttClientCtxCfg_t broker_config;
    void *clt_ctx;
    _u8 *client_id;
    _u8 *usr_name;
    _u8 *usr_pwd;
    bool is_clean;
    _u32 keep_alive_time;
    SlMqttClientCbs_t CallBAcks;
    _i32 num_topics;
    char *topic[SUB_TOPIC_COUNT];
    _u8 qos[SUB_TOPIC_COUNT];
    SlMqttWill_t will_params;
    bool is_connected;
} connect_config;

// MQTT Event Messages Index
typedef enum events
{
	NO_ACTION = -1,
	SUBCRIBE_UPDATE = 0,
	BROKER_DISCONNECTION = 1,
} mqttClientMessages;

/* local module variable declarations */

// Timer Variables
static xTimerHandle     s_mqttTimerStreamHandler        = CMN_POINTER_NULL;            /**< timer handle for streamed data */
static xTimerHandle     s_mqttTimerConfigHandler        = CMN_POINTER_NULL;            /**< timer handle for configuration data */
static uint32_t			     s_mqttTimerStreamValue          = CMN_NUMBER_UINT32_ZERO;		/**< Period in milliseconds for the stream timer */
static uint32_t			     s_mqttTimerConfigValue          = CMN_NUMBER_UINT32_ZERO;		/**< Period in milliseconds for the config timer */

static xTaskHandle      s_mqttTaskClientHandler         = CMN_POINTER_NULL;            /**< task handle for MQTT Client */
static int16_t 			     s_mqttWifiConnectionHandler     = CMN_NUMBER_INT16_ZERO;       /**< hander for wifi connection */
mqttClientMessages           mqttQueue                       = NO_ACTION;

// Publishing topics variables
char mqttPubTopicStream[MQTT_PRINT_BUF_LEN];
char mqttPubTopicReport[MQTT_PRINT_BUF_LEN];
char mqttPubTopicConfig[MQTT_PRINT_BUF_LEN];
const char *mqttPubTopicStream_ptr = PUB_TOPIC_1;
const char *mqttPubTopicReport_ptr = PUB_TOPIC_2;
const char *mqttPubTopicConfig_ptr = PUB_TOPIC_3;

// Subscribe topics variables
char mqttSubTopicSingle[MQTT_PRINT_BUF_LEN];
char mqttSubTppicGroup[MQTT_PRINT_BUF_LEN];
const char *mqttSubTopicSingle_ptr = SUB_TOPIC1;
const char *mqttSubTopicGroup_ptr = SUB_TOPIC2;

// MQTT Print Data Buffer
_i8 mqttPrintBuf[MQTT_PRINT_BUF_LEN];

/* local function definitions */

/**
 * @brief Dummy function
 *
 * @param[in] inBuff
 *     not used
 *
 * @Return 0
 */
static _i32 mqttDummy(_const char *inBuff, ...);

/**
 * @brief stops the timer
 */
static void mqttStopTimer(xTimerHandle TimerHandle);

/**
 * @brief The function that updates the timer period
 *        or stops the timer if an invalid period is entered
 */
static void mqttUpdateTimerPeriod(void);

/**
 * @brief Dummy function
 *
 * @param[in] inBuff
 *     The input buffer containing the data to send
 * @param[in] bufsz
 *     The size of the input buffer
 *
 * @Return CMN_TRUE if the data was sent succesfully
 */
static int mqttClientWrite(unsigned char *inBuff, int bufsz);

/**
 * @brief Defines Mqtt_Pub_Message_Receive event handler.
 *        Client App needs to register this event handler with sl_ExtLib_mqtt_Init
 *        API. Background receive task invokes this handler whenever MQTT Client
 *        receives a Publish Message from the broker.
 *
 * @param[in] application_hndl
 *     unused parameter
 * @param[in] topstr
 *     Pointer to topic of the message
 * @param[in] top_len
 *     Topic length
 * @param[in] payload
 *     Pointer to payload
 * @param[in] pay_len
 *     Payload length
 * @param[in] retain
 *     Tells whether its a Retained message or not
 * @param[in] dup
 *     Tells whether its a duplicate message or not
 * @param[in] qos
 *     Tells the Qos level
 *
 * @Return none
 */
static void mqttRecv(void *application_hndl, _const char  *topstr, _i32 top_len,
          _const void *payload, _i32 pay_len, bool dup,_u8 qos, bool retain);

/**
 * @brief Defines sl_MqttEvt event handler.
 *        Client App needs to register this event handler with sl_ExtLib_mqtt_Init
 *        API. Background receive task invokes this handler whenever MQTT Client
 *        receives an ack(whenever user is in non-blocking mode) or encounters an error.
 *
 * @param[in] application_hndl
 *     unused parameter
 * @param[in] evt
 *     Event that invokes the handler. Event can be of the
 *     following types:
 *     MQTT_ACK - Ack Received
 *     MQTT_ERROR - unknown error
 * @param[in] buf
 *     points to buffer
 * @param[in] len
 *     buffer length
 *
 * @Return none
 */
static void mqttEvt(void *application_hndl,_i32 evt, _const void *buf, _u32 len);

/**
 * @brief Callback event in case of MQTT disconnection
 *
 * @param[in] application_hndl
 *     Handle for the disconnected connection
 *
 * @return none
 */
static void mqttDisconnect(void *application_hndl);

/**
 * @brief Publishes the message on a topic.
 *
 * @param[in] clt_ctx
 *      Client context
 * @param[in] publish_topic
 *     Topic on which the message will be published
 * @param[in] publish_data
 *     The message that will be published
 *
 * @return none
 */
static void publishData(void *clt_ctx, _const char *publish_topic, _u8 *publish_data);

/**
 * @brief Subscribes the MQTT Client to MQTT Topics set by global variables
 *
 * @return none
 */
static void mqttClientSubscribe(void);

/**
 * @brief Unsubcribes the MQTT Client to all MQTT Topics
 *
 * @return none
 */
static void mqttClientUnSubscribe(void);

/**
 * @brief Initializes the MQTT Client;
 *        Connects to the MQTT Broker;
 *        Subscribes to Topics;
 *        Publishes to Topics;
 *
 * @return none
 */
static void mqttClientInit(void);

/**
 * @brief MQTT Client Task - Publishes Data Streams;
 *        Sets Subscribe Topics;
 *        Disconnects from the MQTT Broker;
 *
 * @param[in] application_hndl
 *     unused parameter
 *
 * @Return none
 */
static void mqttClientTask(void *pvParameters);

/**
 * @brief De-intializes and exits the MQTT Client
 *
 * @Return none
 */
static void mqttClientExit(void);

#endif /* MQTT_CLIENT_CH_H_ */

/** ************************************************************************* */
