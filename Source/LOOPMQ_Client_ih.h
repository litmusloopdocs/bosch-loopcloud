/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	LOOPMQ_Client_ih.h
**
**	DESCRIPTION:	Interface header for the MQTT Client module
**
**	PURPOSE:        Contains all the global definitions and macros for the
**                  MQTT Client module
**
**	AUTHOR(S):		Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**  2016.Jan         crk        BCDS         Initial Version
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef LOOPMQ_Client_IH_H_
#define LOOPMQ_Client_IH_H_

/* public interface declaration ********************************************* */

/* interface header files */
#include "simplelink.h"

/* public type and macro definitions */

/**** Functions Inherent to the TI Code, Never teseted Removal ****/
/**** Asychronous Event Handlers ****/

/*!
    \brief This function handles general error events indication

    \param[in]      pDevEvent is the event passed to the handler

    \return         None
*/
void SimpleLinkGeneralEventHandler(SlDeviceEvent_t *pDevEvent);

/*!
    \brief This function handles socket events indication

    \param[in]      pSock is the event passed to the handler

    \return         None
*/
void SimpleLinkSockEventHandler(SlSockEvent_t *pSock);

/**** FreeRTOS User Hook Functions enabled in FreeRTOSConfig.h ****/

/*
    \brief Application defined hook (or callback) function - assert

    \param[in]  pcFile - Pointer to the File Name
    \param[in]  ulLine - Line Number

    \return none
*/
void vAssertCalled(_const _i8 *pcFile, _u32 ulLine);

/*
    \brief Application defined idle task hook

    \param  none

    \return none
 */
void vApplicationIdleHook(void);

/*
    \brief Application defined malloc failed hook

    \param  none

    \return none
 */
void vApplicationMallocFailedHook(void);

/* public function prototype declarations */

/**
 * @brief Sets the connection Handler for the WIFI connection.
 * @param[in] connectionHandler
 *     BLE connection Handler
 * @param[in] flag
 * 	   flag specifying if the transmesion canal shoul be used or not (1/0)
 *
 * @return none
 */
void MQTT_setWifiConnectionHandler(int16_t tcpSocket, int8_t flag);

/**
 * @brief The function initializes the MQTT Module
 *    Initializes the Client, Timers and Tasks
 *
 * @return none
 */
void MQTT_init(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* MQTT_Client_IH_H_ */

/** ************************************************************************* */
