/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	osi.c
**
**	DESCRIPTION:	Source Code for the TI MQTT Client
**
**	PURPOSE:        Contains all the definitions and functions to link the TI
**	                Code with the XDK code to eliminate added modifications
**	                by the user
**
**	AUTHOR(S):	    Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**  2016.Apr         crk        BCDS         Initial Release
**
**
*******************************************************************************/

/* system header files */
#include <stdint.h>

/* interface header files */
#include "BCDS_PowerMgt.h"

/* other header files */
#include "platform.h"

/* own header files */
#include "osi.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************** */

void platform_timer_init(void)
{
	// This is already done.  Do nothing
	return;
}

u32 platform_get_time_in_secs(void)
{
	return PowerMgt_GetSystemTime();
}

int8_t osi_TaskCreate(pdTASK_CODE pxTaskCode, const char * const pcName,
    uint16_t usStackDepth, void * pvParameters, uint32_t uxPriority,
    xTaskHandle *taskHandle)
{
	(void) pvParameters;
	xTaskCreate(pxTaskCode, pcName, usStackDepth, NULL, uxPriority, taskHandle);
	return 0;
}

void osi_TaskDelete(xTaskHandle taskHandle)
{
	vTaskDelete(taskHandle);
}
