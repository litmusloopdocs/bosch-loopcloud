/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	MQTT_Client_ih.h
**
**	DESCRIPTION:	Interface header for the MQTT Client module
**
**	PURPOSE:        Contains all the global definitions and macros for the
**                  MQTT Client module
**
**	AUTHOR(S):		Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**  2016.Jan         crk        BCDS         Initial Version
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef _OSI_H_
#define _OSI_H_

/* public interface declaration ********************************************* */

#include "FreeRTOS.h"
#include "task.h"

/* public type and macro definitions */
#define OSI_WAIT_FOREVER          (0xFFFFFFFF)

/* public global function declarations */
int8_t osi_TaskCreate(pdTASK_CODE pxTaskCode, const char * const pcName,
    uint16_t usStackDepth, void * pvParameters, uint32_t uxPriority,
    xTaskHandle *taskHandle);

void osi_TaskDelete(xTaskHandle taskHandle);

/* public global variable declarations */

/* inline function definitions */

#endif /* _OSI_H_ */
