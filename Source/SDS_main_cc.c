/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	SDS_main_cc.c
**
**	DESCRIPTION:	Main code for the project
**
**	PURPOSE:        Calls all the functions to read the configuration file,
**	                configure the XDK, initialize the sensors, connect to
**	                the wifi, connect to the MQTT server and start streaming
**	                data to the Bosch IoT suite.
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2015.Oct         crk        BCDS         Removed unneccessary code to connect
**                                           to a MQTT Server via wifi (i.e. BLE)
**  2016.Jan         crk        BCDS         Further Consolidation/Cleanup of Code
**
*******************************************************************************/

/* module includes ********************************************************** */

#include <stdio.h>

/* interface header files */
#include "PTD_portDriver_ph.h"
#include "PTD_portDriver_ih.h"
#include "FreeRTOS.h"
#include "timers.h"

/* own header files */
#include "WNS_com_ih.h"
#include "DTA_send_ih.h"
#include "CFG_parser_ih.h"
#include "LOOPMQ_Client_ih.h"
#include "SDS_main_ih.h"

/* Constant definitions ***************************************************** */

/* global variables ********************************************************* */

/* local variables ********************************************************* */

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/**
 * @brief Activate Wifi and MQTT Client
 */
static void sdsActivateWifiMqtt(void) {
	/**** Initialize WIFI ****/
    WNS_initialize();

    /**** Initialize MQTT Client ****/
    MQTT_init();
}

/* global functions ********************************************************* */
/**
 * @brief This function initializes the system.
 *
 * Param[in]  xTimer
 *            timer handler parameter
 */
void appInitSystem(xTimerHandle xTimer)
{
    (void) (xTimer);

    /**** Turn Yellow LED on During Initialization/Configuration ****/
    PTD_pinOutSet(PTD_PORT_LED_YELLOW, PTD_PIN_LED_YELLOW);
    PTD_pinOutClear(PTD_PORT_LED_ORANGE, PTD_PIN_LED_ORANGE);
    PTD_pinOutClear(PTD_PORT_LED_RED, PTD_PIN_LED_RED);

    SDS_init();
}

/**
 * @brief in SDS_main_ih.h header
 */
void SDS_init(void) {
	/**** Initialize the Configuration ****/
	CFG_init();

	/**** Initialize DTA ****/
	DTA_init();

	/**** Initialize the Wifi ****/
	sdsActivateWifiMqtt();
}

/**
 * @brief in SDS_main_ih.h header
 */
void SDS_deinit(void) {
    /**** Un Initialize DTA ****/
    DTA_deinit();

    /**** Un Initialize WIFI ****/
    WNS_deinit();
}

/** ************************************************************************* */
