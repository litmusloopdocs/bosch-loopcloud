/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	SDS_main_ih.h
**
**	DESCRIPTION:	Interface header file for SDS_main_cc.c
**
**	PURPOSE:        Contains Global functions, types, macros and other definitions
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2016.Jan         crk        BCDS         Consolidation/Cleanup of Code
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef XDK110_PSDS_MAIN_IH_H_
#define XDK110_PSDS_MAIN_IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */
#define SDS_BOSCH_IOT_SUITE_REVISION     "  3.0"

/* public function prototype declarations */

/**
 * @brief The function initializes :
 *  - Configuration Data
 *  - Data send module
 *  - Wifi/MQTT module
 */
void SDS_init(void);

/**
 *  @brief This function de-initializes the XDK user application
 */
void SDS_deinit(void);

/* public global variable declarations */

/* inline function definitions */

#endif /* XDK110_PAD_PRINTACCELDATA_IH_H_ */

/** ************************************************************************* */
