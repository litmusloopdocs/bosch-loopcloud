/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	WNS_com_cc.c
**
**	DESCRIPTION:	Manages the WiFi Network
**
**	PURPOSE:        This file contains the implementation for using the WIFI for
**	                exchanging data with the monitoring device
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2016.Jan         crk        BCDS         Updated Connection and Initialization
**                                           Functions for more Reliable Connection;
**                                           Consolidation/Cleanup of Code
**
*******************************************************************************/

/* module includes ********************************************************** */

/* system header files */
#include <stdint.h>

/* interface header files */
#include "PIp.h"
#include "BCDS_WlanConnect.h"
#include "netcfg.h"
#include "BCDS_NetworkConfig.h"
#include "Serval_Ip.h"

/* own header files */
#include "CMN_common_ch.h"
#include "CFG_parser_ih.h"
#include "WNS_com_ch.h"

/* constant definitions ***************************************************** */

/* local variables ********************************************************** */

/* global variables ********************************************************* */
void *g_mac_addr = (void*)s_MacAddress;

/* inline functions ********************************************************* */

/* local functions ********************************************************** */

/* global functions ********************************************************* */

/**
 * @brief in WNS_com_ih.h header
 */
void WNS_initialize(void)
{
	/**** Initialize Variables ****/
	uint8_t _macVal[WNS_MAC_ADDR_LEN];
	uint8_t _macAddressLen = WNS_MAC_ADDR_LEN;
    NCI_ipSettings_t myIpSettings;
    memset(&myIpSettings, (uint32_t) 0, sizeof(myIpSettings));
    int8_t ipAddress[PAL_IP_ADDRESS_SIZE] = {0};
    Ip_Address_T* IpaddressHex = Ip_getMyIpAddr();
    WLI_connectSSID_t connectSSID;
    WLI_connectPassPhrase_t connectPassPhrase;

    /**** Initialize the Wireless Network Driver ****/
    if(WLI_SUCCESS != WLI_init())
    {
        printf("Error occurred initializing WLAN \r\n");
        return;
    }

    /**** Get the SSID and PWD for the WiFi Network ****/
    connectSSID = (WLI_connectSSID_t) CFG_getWlanSsid();
    connectPassPhrase = (WLI_connectPassPhrase_t) CFG_getWlanPwd();
    printf("Connecting to %s \r\n", connectSSID);

    /**** Connect to the Wireless Network ****/
    if (WLI_SUCCESS == WLI_connectWPA(connectSSID, connectPassPhrase, NULL)) {
        NCI_getIpSettings(&myIpSettings);
        *IpaddressHex = Basics_htonl(myIpSettings.ipV4);
        (void)Ip_convertAddrToString(IpaddressHex,(char *)&ipAddress);
        printf("Connected to WPA network successfully. \r\n");
        printf("Ip address of the device: %s \r\n",ipAddress);
    }
    else  {
        printf("Error occurred connecting %s \r\n ", connectSSID);
        return;
    }

    /**** Get the MAC Address ****/
    memset(_macVal, CMN_NUMBER_UINT8_ZERO, WNS_MAC_ADDR_LEN);
    int8_t _status = sl_NetCfgGet(SL_MAC_ADDRESS_GET,
    								NULL,
    					            &_macAddressLen,
    			                    (uint8_t *) _macVal);

	if (WNS_FAILED == _status) {
		printf("sl_NetCfgGet Failed\r\n");
	}
	else {
		/**** Store the MAC Address into the Global Variable ****/
        memset(s_MacAddress, CMN_NUMBER_UINT8_ZERO, strlen(s_MacAddress));
    	sprintf(s_MacAddress,"%02X:%02X:%02X:%02X:%02X:%02X",_macVal[0],
    				                             	         _macVal[1],
    					                                     _macVal[2],
    					                                     _macVal[3],
    					                                     _macVal[4],
    					                                     _macVal[5]);
    	printf("MAC address of the device: %s \r\n",s_MacAddress);
	}
}

/**
 * @brief in WNS_com_ih.h header
 */
void WNS_deinit(void)
{
    ;/**** DO NOTHING ****/
}
/** ************************************************************************* */
