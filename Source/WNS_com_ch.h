/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	WNS_com_ch.h
**
**	DESCRIPTION:	Local Header for WNS_com_cc.c source file
**
**	PURPOSE:        Contains the local macro, typedef, variables and function
**	                definitions for the WNS module
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2016.Jan         crk        BCDS         Consolidation/Cleanup of Code
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef WNS_COM_CH_H_
#define WNS_COM_CH_H_

/* local interface declaration ********************************************** */

/* own header files */
#include "WNS_com_ih.h"

/* local type and macro definitions */
#define WNS_FAILED 			   			INT32_C(-1)                 /**< Macro used to return failure message*/
#define WNS_MAC_ADDR_LEN 	   			UINT8_C(6)                  /**< Macro used to specify MAC address length*/

/* local module variable declarations */
static char s_MacAddress[]    		  = WNS_DEFAULT_MAC;    /**< To keep the MAC adress returned by the wifi network device */

/* local function prototype declarations */

/* local inline function definitions */

#endif /* WNS_COM_CH_H_ */

/** ************************************************************************* */
