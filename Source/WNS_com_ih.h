/******************************************************************************
**	COPYRIGHT (c) 2016		Bosch Connected Devices and Solutions GmbH
**
**	The use of this software is subject to the XDK SDK EULA
**
*******************************************************************************
**
**	OBJECT NAME:	WNS_com_ih.h
**
**	DESCRIPTION:	Public Interface Header for WNS_com_cc.c source file
**
**	PURPOSE:        Contains the public macro, typedef, variables and function
**	                definitions for the WNS module
**
**	AUTHOR(S):      Bosch Connected Devices and Solutions GmbH (BCDS)
**
**	Revision History:
**
**	Date			 Name		Company      Description
**	2015             unknown    BCDS         Initial Release (DMS_demoSensorMonitor)
**  2016.Jan         crk        BCDS         Consolidation/Cleanup of Code
**
*******************************************************************************/

/* header definition ******************************************************** */
#ifndef WNS_COM_IH_H_
#define WNS_COM_IH_H_

/* public interface declaration ********************************************* */

/* public type and macro definitions */
#define WNS_DEFAULT_MAC		            "00:00:00:00:00:00"         /**< Macro used to specify the Default MAC Address*/

/* public global variable declarations */
extern void *g_mac_addr;

/* public function prototype declarations */

/**
 *  @brief Function to connect to wifi network and obtain IP address,
 *         after that waits for incomming TCP connections
 *
 *  @return none
 */
void WNS_initialize(void);

/**
 * @brief Uninit the WNS module
 */
void WNS_deinit(void);

/* inline function definitions */

#endif /* WNS_COM_IH_H_ */

/** ************************************************************************* */
